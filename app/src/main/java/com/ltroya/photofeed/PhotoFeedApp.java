package com.ltroya.photofeed;

import android.app.Application;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.firebase.client.Firebase;
import com.ltroya.photofeed.domain.di.DomainModule;
import com.ltroya.photofeed.libs.di.LibsModule;
import com.ltroya.photofeed.login.di.DaggerLoginComponent;
import com.ltroya.photofeed.login.di.LoginComponent;
import com.ltroya.photofeed.login.di.LoginModule;
import com.ltroya.photofeed.login.ui.LoginView;
import com.ltroya.photofeed.main.di.DaggerMainComponent;
import com.ltroya.photofeed.main.di.MainComponent;
import com.ltroya.photofeed.main.di.MainModule;
import com.ltroya.photofeed.main.ui.MainView;
import com.ltroya.photofeed.photolist.di.DaggerPhotoListComponent;
import com.ltroya.photofeed.photolist.di.PhotoListComponent;
import com.ltroya.photofeed.photolist.di.PhotoListModule;
import com.ltroya.photofeed.photolist.ui.PhotoListFragment;
import com.ltroya.photofeed.photolist.ui.PhotoListView;
import com.ltroya.photofeed.photolist.ui.adapters.OnItemClickListener;
import com.ltroya.photofeed.photomap.di.DaggerPhotoMapComponent;
import com.ltroya.photofeed.photomap.di.PhotoMapComponent;
import com.ltroya.photofeed.photomap.di.PhotoMapModule;
import com.ltroya.photofeed.photomap.ui.PhotoMapFragment;
import com.ltroya.photofeed.photomap.ui.PhotoMapView;

public class PhotoFeedApp extends Application {
    private final static String EMAIL_KEY = "email";
    private final static String SHARED_PREFERENCES_NAME = "UserPrefs";
    private final static String FIREBASE_URL = "https://pfeed.firebaseio.com/";

    private DomainModule domainModule;
    private PhotoFeedAppModule photoFeedAppModule;

    @Override
    public void onCreate() {
        super.onCreate();
        initFirebase();
        initModules();
    }

    private void initModules() {
        photoFeedAppModule = new PhotoFeedAppModule(this);
        domainModule = new DomainModule(FIREBASE_URL);
    }

    private void initFirebase() {
        Firebase.setAndroidContext(this);
    }


    public String getEmailKey() {
        return EMAIL_KEY;
    }

    public String getSharedPreferencesName() {
        return SHARED_PREFERENCES_NAME;
    }

    public String getFirebaseUrl() {
        return FIREBASE_URL;
    }

    public LoginComponent getLoginComponent(LoginView view) {
        return DaggerLoginComponent
                .builder()
                .photoFeedAppModule(photoFeedAppModule)
                .domainModule(domainModule)
                .libsModule(new LibsModule(null))
                .loginModule(new LoginModule(view))
                .build();
    }

    public MainComponent getMainComponent(MainView view, FragmentManager manager, Fragment[] fragments, String[] titles) {
        return DaggerMainComponent
                .builder()
                .photoFeedAppModule(photoFeedAppModule)
                .domainModule(domainModule)
                .libsModule(new LibsModule(null))
                .mainModule(new MainModule(view, titles, fragments, manager))
                .build();
    }

    public PhotoListComponent getPhotoListComponent(PhotoListFragment fragment, PhotoListView view, OnItemClickListener onItemClickListener) {
        return DaggerPhotoListComponent
                .builder()
                .photoFeedAppModule(photoFeedAppModule)
                .domainModule(domainModule)
                .libsModule(new LibsModule(fragment))
                .photoListModule(new PhotoListModule(view, onItemClickListener))
                .build();
    }

    public PhotoMapComponent getPhotoMapComponent(PhotoMapFragment fragment, PhotoMapView view) {
        return DaggerPhotoMapComponent
                .builder()
                .photoFeedAppModule(photoFeedAppModule)
                .domainModule(domainModule)
                .libsModule(new LibsModule(fragment))
                .photoMapModule(new PhotoMapModule(view))
                .build();
    }
}
