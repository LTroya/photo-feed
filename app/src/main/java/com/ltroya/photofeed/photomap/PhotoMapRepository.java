package com.ltroya.photofeed.photomap;

public interface PhotoMapRepository {
    void subscribe();
    void unsubscribe();
}
