package com.ltroya.photofeed.photomap.ui;

import com.ltroya.photofeed.entities.Photo;

public interface PhotoMapView {
    void addPhoto(Photo photo);
    void removePhoto(Photo photo);
    void onPhotosError(String error);
}
