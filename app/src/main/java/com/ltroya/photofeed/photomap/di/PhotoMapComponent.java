package com.ltroya.photofeed.photomap.di;

import com.ltroya.photofeed.PhotoFeedAppModule;
import com.ltroya.photofeed.domain.di.DomainModule;
import com.ltroya.photofeed.libs.di.LibsModule;
import com.ltroya.photofeed.photomap.ui.PhotoMapFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {PhotoMapModule.class, DomainModule.class, LibsModule.class, PhotoFeedAppModule.class})
public interface PhotoMapComponent {
    void inject(PhotoMapFragment fragment);
}
