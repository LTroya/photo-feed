package com.ltroya.photofeed.photomap.di;

import com.ltroya.photofeed.domain.FirebaseAPI;
import com.ltroya.photofeed.libs.base.EventBus;
import com.ltroya.photofeed.main.MainPresenterImpl;
import com.ltroya.photofeed.photomap.PhotoMapInteractor;
import com.ltroya.photofeed.photomap.PhotoMapInteractorImpl;
import com.ltroya.photofeed.photomap.PhotoMapPresenter;
import com.ltroya.photofeed.photomap.PhotoMapPresenterImpl;
import com.ltroya.photofeed.photomap.PhotoMapRepository;
import com.ltroya.photofeed.photomap.PhotoMapRepositoryImpl;
import com.ltroya.photofeed.photomap.ui.PhotoMapView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PhotoMapModule {
    private PhotoMapView view;

    public PhotoMapModule(PhotoMapView view) {
        this.view = view;
    }

    @Provides @Singleton
    PhotoMapView providesPhotoMapView() {
        return this.view;
    }

    @Provides @Singleton
    PhotoMapPresenter providesPhotoMapPresenter(EventBus eventBus, PhotoMapView view, PhotoMapInteractor interactor) {
        return new PhotoMapPresenterImpl(eventBus, view, interactor);
    }

    @Provides @Singleton
    PhotoMapInteractor providesPhotoMapInteractor(PhotoMapRepository repository) {
        return new PhotoMapInteractorImpl(repository);
    }

    @Provides @Singleton
    PhotoMapRepository providesPhotoMapRepository(EventBus eventBus, FirebaseAPI firebaseAPI) {
        return new PhotoMapRepositoryImpl(eventBus, firebaseAPI);

    }
}
