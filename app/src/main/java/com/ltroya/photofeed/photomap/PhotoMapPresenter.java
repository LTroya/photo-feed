package com.ltroya.photofeed.photomap;

import com.ltroya.photofeed.photomap.events.PhotoMapEvent;

public interface PhotoMapPresenter {
    void onCreate();
    void onDestroy();

    void subscribe();
    void unsubscribe();

    void onEventMainThread(PhotoMapEvent event);
}
