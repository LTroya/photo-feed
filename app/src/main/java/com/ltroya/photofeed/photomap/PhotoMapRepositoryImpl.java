package com.ltroya.photofeed.photomap;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.ltroya.photofeed.domain.FirebaseAPI;
import com.ltroya.photofeed.domain.FirebaseEventListenerCallback;
import com.ltroya.photofeed.entities.Photo;
import com.ltroya.photofeed.libs.base.EventBus;
import com.ltroya.photofeed.photolist.events.PhotoListEvent;
import com.ltroya.photofeed.photomap.events.PhotoMapEvent;

public class PhotoMapRepositoryImpl implements PhotoMapRepository{
    private EventBus eventBus;
    private FirebaseAPI firebaseAPI;

    public PhotoMapRepositoryImpl(EventBus eventBus, FirebaseAPI firebaseAPI) {
        this.eventBus = eventBus;
        this.firebaseAPI = firebaseAPI;
    }

    @Override
    public void subscribe() {
        firebaseAPI.subscribe(new FirebaseEventListenerCallback() {
            @Override
            public void onChildAdded(DataSnapshot snapshot) {
                Photo photo = snapshot.getValue(Photo.class);
                photo.setId(snapshot.getKey());

                String email = firebaseAPI.getAuthEmail();
                boolean isPublishedByMe = photo.getEmail().equals(email);
                photo.setPublishedByMe(isPublishedByMe);
                post(PhotoMapEvent.READ_EVENT, photo);
            }

            @Override
            public void onChildRemoved(DataSnapshot snapshot) {
                Photo photo = snapshot.getValue(Photo.class);
                photo.setId(snapshot.getKey());

                post(PhotoMapEvent.DELETE_EVENT, photo);
            }

            @Override
            public void onCancelled(FirebaseError error) {
                post(PhotoMapEvent.READ_EVENT, error.getMessage());
            }
        });
    }

    @Override
    public void unsubscribe() {
        firebaseAPI.unsubscribe();
    }

    private void post(int type, String error) {
        post(type, error, null);
    }

    private void post(int type, Photo photo) {
        post(type, null, photo);
    }

    private void post(int type, String error, Photo photo) {
        PhotoMapEvent event = new PhotoMapEvent();
        event.setType(type);
        event.setError(error);
        event.setPhoto(photo);
        eventBus.post(event);
    }
}
