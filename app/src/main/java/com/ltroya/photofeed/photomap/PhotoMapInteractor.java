package com.ltroya.photofeed.photomap;

public interface PhotoMapInteractor {
    void subscribe();
    void unsubscribe();
}
