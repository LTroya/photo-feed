package com.ltroya.photofeed.login;

public interface LoginInteractor {
    void doSignUp(String email, String password);
    void doSignIn(String email, String password);
}
