package com.ltroya.photofeed.login;

import com.ltroya.photofeed.login.events.LoginEvent;

public interface LoginPresenter {
    void onCreate();
    void onDestroy();
    void validateLogin(String email, String password);
    void registerNewuser(String email, String password);
    void onEventMainThread(LoginEvent event);
}
