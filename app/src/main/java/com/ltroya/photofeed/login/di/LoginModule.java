package com.ltroya.photofeed.login.di;

import com.ltroya.photofeed.domain.FirebaseAPI;
import com.ltroya.photofeed.libs.base.EventBus;
import com.ltroya.photofeed.login.LoginInteractor;
import com.ltroya.photofeed.login.LoginInteractorImpl;
import com.ltroya.photofeed.login.LoginPresenter;
import com.ltroya.photofeed.login.LoginPresenterImpl;
import com.ltroya.photofeed.login.LoginRepository;
import com.ltroya.photofeed.login.LoginRepositoryImpl;
import com.ltroya.photofeed.login.ui.LoginView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {
    LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides @Singleton
    LoginView providesLoginView() {
        return this.view;
    }

    @Provides @Singleton
    LoginPresenter providesLoginPresenter(EventBus eventBus, LoginView loginView, LoginInteractor loginInteractor) {
        return new LoginPresenterImpl(eventBus, loginView, loginInteractor);
    }

    @Provides @Singleton
    LoginInteractor providesLoginInteractor(LoginRepository loginRepository) {
        return new LoginInteractorImpl(loginRepository);
    }

    @Provides @Singleton
    LoginRepository providesLoginRepository(FirebaseAPI firebaseAPI, EventBus eventBus) {
        return new LoginRepositoryImpl(firebaseAPI, eventBus);
    }
}
