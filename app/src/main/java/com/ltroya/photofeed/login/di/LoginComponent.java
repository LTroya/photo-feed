package com.ltroya.photofeed.login.di;

import com.ltroya.photofeed.PhotoFeedAppModule;
import com.ltroya.photofeed.domain.di.DomainModule;
import com.ltroya.photofeed.libs.di.LibsModule;
import com.ltroya.photofeed.login.ui.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LoginModule.class, DomainModule.class, LibsModule.class, PhotoFeedAppModule.class})
public interface LoginComponent {
    void inject(LoginActivity activity);
}
