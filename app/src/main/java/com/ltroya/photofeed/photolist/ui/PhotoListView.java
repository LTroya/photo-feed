package com.ltroya.photofeed.photolist.ui;

import com.ltroya.photofeed.entities.Photo;

public interface PhotoListView {
    void showList();
    void hideList();
    void showProgress();
    void hideProgress();

    void addPhoto(Photo photo);
    void removePhoto(Photo photo);
    void onPhotosError(String error);
}
