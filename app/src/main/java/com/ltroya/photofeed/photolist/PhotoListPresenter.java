package com.ltroya.photofeed.photolist;

import com.ltroya.photofeed.entities.Photo;
import com.ltroya.photofeed.photolist.events.PhotoListEvent;

public interface PhotoListPresenter {
    void onCreate();
    void onDestroy();

    void subscribe();
    void unsubscribe();

    void removePhoto(Photo photo);
    void onEventMainThread(PhotoListEvent event);
}
