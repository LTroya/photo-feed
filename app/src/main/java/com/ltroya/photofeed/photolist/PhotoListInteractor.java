package com.ltroya.photofeed.photolist;

import com.ltroya.photofeed.entities.Photo;

public interface PhotoListInteractor {
    void subscribe();
    void unsubscribe();
    void removePhoto(Photo photo);
}
