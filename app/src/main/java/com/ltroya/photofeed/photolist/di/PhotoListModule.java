package com.ltroya.photofeed.photolist.di;

import com.ltroya.photofeed.domain.FirebaseAPI;
import com.ltroya.photofeed.domain.Util;
import com.ltroya.photofeed.entities.Photo;
import com.ltroya.photofeed.libs.base.EventBus;
import com.ltroya.photofeed.libs.base.ImageLoader;
import com.ltroya.photofeed.photolist.PhotoListInteractor;
import com.ltroya.photofeed.photolist.PhotoListInteractorImpl;
import com.ltroya.photofeed.photolist.PhotoListPresenter;
import com.ltroya.photofeed.photolist.PhotoListPresenterImpl;
import com.ltroya.photofeed.photolist.PhotoListRepository;
import com.ltroya.photofeed.photolist.PhotoListRepositoryImpl;
import com.ltroya.photofeed.photolist.ui.PhotoListView;
import com.ltroya.photofeed.photolist.ui.adapters.OnItemClickListener;
import com.ltroya.photofeed.photolist.ui.adapters.PhotoListAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PhotoListModule {
    private PhotoListView view;
    OnItemClickListener onItemClickListener;

    public PhotoListModule(PhotoListView view, OnItemClickListener onItemClickListener) {
        this.view = view;
        this.onItemClickListener = onItemClickListener;
    }

    @Provides @Singleton
    PhotoListView providesPhotoListView() {
        return this.view;
    }

    @Provides @Singleton
    PhotoListPresenter providesPhotoListPresenter(EventBus eventBus, PhotoListView view, PhotoListInteractor interactor) {
        return new PhotoListPresenterImpl(eventBus, view, interactor);
    }

    @Provides @Singleton
    PhotoListInteractor providesPhotoListInteractor(PhotoListRepository repository) {
        return new PhotoListInteractorImpl(repository);
    }

    @Provides @Singleton
    PhotoListRepository providesPhotoListRepository(EventBus eventBus, FirebaseAPI firebaseAPI) {
        return new PhotoListRepositoryImpl(eventBus, firebaseAPI);
    }

    @Provides @Singleton
    PhotoListAdapter providesPhotoListAdapter(Util utils, List<Photo> photoList, ImageLoader imageLoader, OnItemClickListener onItemClickListener) {
        return new PhotoListAdapter(utils, photoList, imageLoader, onItemClickListener);
    }

    @Provides @Singleton
    OnItemClickListener providesOnItemClickListener() {
        return this.onItemClickListener;
    }

    @Provides @Singleton
    List<Photo> providesPhotoList() {
        return new ArrayList<Photo>();
    }
}
