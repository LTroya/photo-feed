package com.ltroya.photofeed.photolist.di;

import com.ltroya.photofeed.PhotoFeedApp;
import com.ltroya.photofeed.PhotoFeedAppModule;
import com.ltroya.photofeed.domain.di.DomainModule;
import com.ltroya.photofeed.libs.di.LibsModule;
import com.ltroya.photofeed.photolist.ui.PhotoListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {PhotoListModule.class, DomainModule.class, LibsModule.class, PhotoFeedAppModule.class})
public interface PhotoListComponent {
    void inject(PhotoListFragment fragment);
}
