package com.ltroya.photofeed.domain.di;

import com.ltroya.photofeed.PhotoFeedAppModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {DomainModule.class, PhotoFeedAppModule.class})
public interface DomainComponent {
}
