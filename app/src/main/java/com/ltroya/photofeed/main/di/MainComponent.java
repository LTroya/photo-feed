package com.ltroya.photofeed.main.di;

import com.ltroya.photofeed.PhotoFeedAppModule;
import com.ltroya.photofeed.domain.di.DomainModule;
import com.ltroya.photofeed.libs.di.LibsModule;
import com.ltroya.photofeed.main.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {MainModule.class, DomainModule.class, LibsModule.class, PhotoFeedAppModule.class})
public interface MainComponent {
    void inject(MainActivity activity);
}
