package com.ltroya.photofeed.main.di;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ltroya.photofeed.domain.FirebaseAPI;
import com.ltroya.photofeed.libs.base.EventBus;
import com.ltroya.photofeed.libs.base.ImageStorage;
import com.ltroya.photofeed.main.MainPresenter;
import com.ltroya.photofeed.main.MainPresenterImpl;
import com.ltroya.photofeed.main.MainRepository;
import com.ltroya.photofeed.main.MainRepositoryImpl;
import com.ltroya.photofeed.main.SessionInteractor;
import com.ltroya.photofeed.main.SessionInteractorImpl;
import com.ltroya.photofeed.main.UploadInteractor;
import com.ltroya.photofeed.main.UploadInteractorImpl;
import com.ltroya.photofeed.main.ui.MainView;
import com.ltroya.photofeed.main.ui.adapters.MainSectionPagerAdapter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    private MainView view;
    private String[] titles;
    private Fragment[] fragments;
    private FragmentManager fragmentManager;

    public MainModule(MainView view, String[] titles, Fragment[] fragments, FragmentManager fragmentManager) {
        this.view = view;
        this.titles = titles;
        this.fragments = fragments;
        this.fragmentManager = fragmentManager;
    }

    @Provides @Singleton
    MainView providesMainView() {
        return this.view;
    }

    @Provides @Singleton
    MainPresenter providesMainPresenter(MainView view, EventBus eventBus, UploadInteractor uploadInteractor, SessionInteractor sessionInteractor) {
        return new MainPresenterImpl(view, eventBus, uploadInteractor, sessionInteractor);
    }

    @Provides @Singleton
    UploadInteractor providesUploadInteractor(MainRepository repository) {
        return new UploadInteractorImpl(repository);
    }

    @Provides @Singleton
    SessionInteractor providesSessionInteractor(MainRepository repository) {
        return new SessionInteractorImpl(repository);
    }

    @Provides @Singleton
    MainRepository providesMainRepository(EventBus eventBus, FirebaseAPI firebaseAPI, ImageStorage imageStorage) {
        return new MainRepositoryImpl(eventBus, firebaseAPI, imageStorage);
    }

    @Provides @Singleton
    MainSectionPagerAdapter providesMainSectionPagerAdapter(FragmentManager fragmentManager, String[] titles, Fragment[] fragments) {
        return new MainSectionPagerAdapter(fragmentManager, titles, fragments);
    }

    @Provides @Singleton
    FragmentManager providesAdapterFragmentManager() {
        return this.fragmentManager;
    }

    @Provides @Singleton
    Fragment[]  providesFragmentArrayForAdapter() {
        return this.fragments;
    }

    @Provides @Singleton
    String[] providesStringArrayForAdapter() {
        return this.titles;
    }
}
