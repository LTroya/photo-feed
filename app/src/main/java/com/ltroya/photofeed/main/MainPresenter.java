package com.ltroya.photofeed.main;

import android.location.Location;

import com.ltroya.photofeed.main.events.MainEvent;

/**
 * Created by LTroya on 7/11/2016 AD.
 */
public interface MainPresenter {
    void onCreate();
    void onDestroy();

    void logout();
    void uploadPhoto(Location location, String path);
    void onEventMainThread(MainEvent event);
}
