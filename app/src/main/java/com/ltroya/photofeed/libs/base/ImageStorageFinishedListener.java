package com.ltroya.photofeed.libs.base;

public interface ImageStorageFinishedListener {
    void onSucces();
    void onError(String error);
}
