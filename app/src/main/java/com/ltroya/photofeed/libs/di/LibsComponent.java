package com.ltroya.photofeed.libs.di;

import com.ltroya.photofeed.PhotoFeedApp;
import com.ltroya.photofeed.PhotoFeedAppModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, PhotoFeedAppModule.class})
public interface LibsComponent {
}
